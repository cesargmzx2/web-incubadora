<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Bienvenido al Registro</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> <!--Estilo de la pagina con los datos centrados, 
sde CSS --->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body >
	<header style="text-align: center;">    <!--Alineamos el texto de una forma central para que se vea un orden y una estetica-->
          <div class="form">
          <form action="db.php" method="POST" name="fcontacto">  <!--Creamos un formulario apartir de esta linea-->
    
                
		<p>Nombre del evento </p>
    <input type="text" name="nombreEvento" placeholder="Nombre del evento" required=""><br><br>

       <span> Tipo de evento </span><br>                                        
      <select type="option" id="select" name="tipoDeEvento" required=""> <br> 
        <option type="option" name="tipoDeEvento" value="Conferencia" id="tipoDeEvento_0" onclick="mostrarReferencia();"/> Conferencia</option>
        <option type="option" name="tipoDeEvento" value="Taller" id="tipoDeEvento_1" onclick="mostrarReferencia();"/> Taller</option>
        <option type="option" name="tipoDeEvento" value="Curso" id="tipoDeEvento_2" onclick="mostrarReferencia();"/> Curso</option>
        <option type="option" name="tipoDeEvento" value="Mesa Redonda" id="tipoDeEvento_3" onclick="mostrarReferencia();"/> Mesa redonda</option>
        <option  type="option" name="tipoDeEvento" value="Congreso" id="tipoDeEvento_4" onclick="mostrarReferencia();"/> Congreso</option>
  
       </select><br>
       <p>Otro:</p>
       <input type="text" name="tipoDeEvento" placeholder="Otro"><br>

       <!--<input type="radio" name="tipoDeEvento" value="Conferencia" id="tipoDeEvento_0" onclick="mostrarReferencia();"/> Conferencia
       <input type="radio" name="tipoDeEvento" value="Taller" id="tipoDeEvento_1" onclick="mostrarReferencia();"/> Taller
       <input type="radio" name="tipoDeEvento" value="Curso" id="tipoDeEvento_2" onclick="mostrarReferencia();"/> Curso<br>
       <input type="radio" name="tipoDeEvento" value="Mesa Redonda" id="tipoDeEvento_3" onclick="mostrarReferencia();"/> Mesa Redonda
       <input type="radio" name="tipoDeEvento" value="Congreso" id="tipoDeEvento_4" onclick="mostrarReferencia();"/> Congreso
       <input type="radio" name="tipoDeEvento" value="Otro" id="tipoDeEvento_5"  onclick="mostrarReferencia();"/> Otro<br>-->

      
       <!--<div id="desdeotro" style="display:none;"> Bloque donde especificamos que al aparecer el campo "oculto" indica lo que puedes hacer en el
        <p>Escribe el tipo de evento que deseas crear:</p>


          <p><input type="text" name="desdeotro" class="input" placeholder="Tipo de Evento" /></p>
       </div>
       funcion javascript en la cabecera del documento
<script type="text/javascript">
function mostrarReferencia(){
//Si la opcion con id Conocido_5 (dentro del documento > formulario con name fcontacto >     y a la vez dentro del array de Conocido) esta activada
if (document.fcontacto.tipoDeEvento[5].checked == true) {
//muestra (cambiando la propiedad display del estilo) el div con id 'desdeotro'
document.getElementById('desdeotro').style.display='block';
//por el contrario, si no esta seleccionada
} else {
//oculta el div con id 'desdeotro'
document.getElementById('desdeotro').style.display='none';
}
}
</script><br>-->
  		 	<span> Fecha de inicio</span><br>
  			<input type="date" id="date"name="fechaInicio" placeholder="Fecha de inicio" required=""><br><br> 
      
        <span>Fecha de término</span><br>
        <input type="date"id="date" name="fechaTermino"><br><br>
        <span>Tipo de sede</span><br>
          <select type="option" id="select" name="tipoSede" required="">                   <!--Boton que despliega las sedes disponibles-->
          <option>Nacional</option>
          <option>Internacional</option>
          </select><br><br>
          <span>Lugar</span><br>
          <select type="option" id="select" name="lugar" required="">           <!--Boton que despliega el lugar del evento-->

             <option>Sala de Videoconferencias UIM II</option>
            <option>Aula Magna UIM II</option>
            <option>Unidad de Congresos UIM II</option>
          </select> <br><br>
          <p>Otro Lugar para el Evento:</p>
          <input type="text" name="lugar" placeholder="Escribe Aqui"><br>

          <!--<input type="text" name="otroLugar" placeholder="Indica un lugar distinto"><br><br>-->

          <span>Tipo de participación</span><br>
          <select type="option" id="select" name="tipoDeParticipacion" required="">
            <option>Organizador</option>
            <option>Colaborador</option>
            <option>Invitado</option>
          </select><br>
          <p>Otro tipo de participacion:</p>
          <input type="text" name="tipoDeParticipacion" placeholder="Escribe Aqui:">
          <br>
          <!--<input type="text" name="otroTipoDeParticipacion" placeholder="Escribe aqui">-->

          <p>Descripción</p>
          <input type="text" name="descripcion" placeholder="Escribe aqui">
          <p>Cupo</p>
          <input type="number" name="cupo" placeholder="Escribe aqui" min="1" max="1000">
          <p>Numero de sesiones</p>
            <input type="number" name="numeroDeSesiones" min="1" max="400">
          <p>Nombre del ponente</p>
          <input type="text" name="nombrePonente" placeholder="Nombre" required=""><br><br>
          <span> Número de registros minimos</span><br>
          <input type="Number" id="numero " name="registrosMinimos" min="1" max="1000" /><br><br>
          <span> Número de regostros máximos</span><br>
          <input type="Number" id="numero " name="registrosMaximos" min="1" max="1000" /><br>

        <p>Activo</p>
        <input type=radio  name="habilitar" id="Si">SI
        <!--<label for="Si">Si</label>-->
        <input type=radio  name="habilitar" id="no">NO<br>
       <!-- <label for="No">No</label><br><br>-->

          <span>Hora de inicio</span><br><br>
  			 <input type="time" id="time" name="hora"><br><br>           <!--Campo para indicar la hora-->
         <a href="index.html">
         <input type="submit" name="Salir" value="Salir"></a>
         <input type="submit" name="Enviar" value="Enviar" onclick="confirmForm()" ></a><br><br>
            



              </form>
            </div>
  		 </header>

</body>
</html>

