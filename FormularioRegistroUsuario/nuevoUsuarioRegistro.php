<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="assets/css/stilos.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<script type="text/javascript">
		function deshabilitar ()
		{
			
			if (document.getElementById("no1").checked)
			{
				document.getElementById("comUNAM").disabled=true;
				document.getElementById("carrera").disabled=true;
					
				document.getElementById("no_cta").disabled=true;			    
			        

			}
			else
			{
				document.getElementById("comUNAM").disabled=false;
				document.getElementById("carrera").disabled=false;
				document.getElementById("no_cta").disabled=false;
			}

		}
		/*function primMayNom()
		{
			var nombre = document.getElementById("name01");
			var nombres = nombre.split (" ");
			var tamañoA = nombres.lentg;
			document.getElementById("name01").value=primMayAp(nombres[0])+" "+ primMayAp(nombres[1]);

		}		

		function primMayAp (someId)
		{
			var nomb = document.getElementById(someId).value;
			res= nomb.toLowerCase();
			//document.getElementById(someId).value =res.charAt(0).toUpperCase() + res.slice(1);
			document.getElementById(someId).value =res.charAt(0).toUpperCase()+res.slice(1);
			

			//document.getElementById("name01").value =nomb.toUpperCase();
			//nomb.toUpperCase();
			//res= nomb.toUpperCase();			
			//nomb.innerHTML=res;
		}*/
		
	</script> 
		<!--<script type="text/javascript">
		function deshabilitar ()
		{
			var sec1 = document.getElementById("ComUNAM");
			sec1.style.display= 'none';
		}
	</script> -->

	<title>Nuevo usuario DPEICI</title>
	
</head>
<body>

	<center><h1>Registro de usuarios.</h1></center>	
	<div class="form">
	<!--<form method="GET" action="dbInsertNewUser.php"> -->
    <!--<form method=GET >-->
    
     <form action="dbInsertNewUser.php" method="GET" name="indexRegUs" > 	

		<section id="Registro" >		
		<center>			
			<p>Nombre(s):</p>
    		<input type="text" id="name01" name="nombre" placeholder="Nombre(s)" required="" > <br><br>
    		<p>Apellido paterno:</p>
    		<input type="text" name="apPa" id="apPa" placeholder="Apellido paterno" required="" > <br><br>
    		<p>Apellido materno:</p>
    		<input type="text" name="apMa" id="apMa" placeholder="Apellido materno" required="" > <br><br>
    		<p>Telefono:</p>
    		<input type="text" name="telefono" placeholder="Teléfono" required=""> <br><br>
    		<p>Eres comunidad UNAM:</p>
    		<div id="ComU" onclick="deshabilitar()">
    		  <input type="radio" name="comunidad" value="1" id="si1" checked="checked" > Sí<br>
              <input type="radio" name="comunidad" value="2" id="no1"  >No<br>
            </div>  
    		<br>

         <!--<input type="submit" name="Enviar" value="Enviar" onclick="confirmForm()" >-->

			<div >
				<p>Tipo de comunidad:</p>			
    		<select name="tipoCom"   id="comUNAM">
  				<option value="Alumno">Alumno</option>
  				<option value="ExAlumno">Ex-Alumno</option>
 			 	<option value="Profesor">Profesor</option>
  				<option value="Investigador">Investigador</option>
  				<option value="Administrativo">Administrativo</option>
  				<option value="Otro">Otro</option>  				
			</select> 
			<br>		
			<p>Carrera:</p> 	
    		<select name="carrera"   id="carrera">
  				<option value="Derecho">Derecho</option>
  				<option value="Arquitectura">Arquitectura</option>
 			 	<option value="Diseño Gráfico">Diseño Gráfico</option>
  				<option value="Actuaría">Actuaría</option>
  				<option value="Ingeniería Civil">Ingeniería Civil</option>
  				<option value="Matemáticas Aplicadas y Computación">Matemáticas Aplicadas y Computación</option>  
  				<option value="Ciencias Políticas y Administración Pública">Ciencias Políticas y Administración Pública</option> 
  				<option value="Economía">Economía</option> 
  				<option value="Relaciones Internacionales">Relaciones Internacionales</option> 
  				<option value="Sociología">Sociología</option> 
  			    <option value="Comunicación">Comunicación</option> 
  				<option value="Enseñanza de Inglés">Enseñanza de Inglés</option> 
  				<option value="Filosofía">Filosofía</option> 
  				<option value="Historia">Historia</option> 
  				<option value="Lengua y Literatura Hispánicas">Lengua y Literatura Hispánicas</option> 
  				<option value="Pedagogía">Pedagogía</option> 
  				<option value="LICEL">Enseñanza de Idiomas como Lengua Extranjera (LICEL)</option> 
  				<option value="Otra">Otra</option> 				
			</select> <br>
			<p>Numero de cuenta:</p>
			<input type="text" id="no_cta" name="no_cta" placeholder="Numero de cuenta UNAM" required="" > <br>				
			</div>		
			<p>¿Cómo te enteraste del evento?</p>
			<select name="como_me_entere" id="como_me_entere"> 
			<option value="Redes Sociales">Redes sociales</option> 		
			<option value="Correo">Correo</option> 		
			<option value="Recomendación">Recomendación</option> 		
			<option value="Medio impreso">Medio impreso</option> 		
			<option value="Otro">Otro</option> 			
			</select>
			<br>
			
			<center><p>Aviso de privacidad.</p></center>
			<p>Para prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiendonos tratarlos debidamente.
			</p>
			<p>Atte: Departamento de Proyección Empresarial UNAM FES-Acatlán.</p> <br><br>
			<p>Acepto los términos y condiciones.</p>
			<!-- imagen de terminos y condiciones-->
    		  <input type="radio" name="termCond" value="Si" > Sí<br>
              <input type="radio" name="termCond" value="No">No<br>
    		<input type="submit" name="registrar" value="Terminar registro"></a>
         <!--<input type="submit" name="Enviar" value="Enviar" onclick="confirmForm()" >-->
    
		</center>
	</section>
    <br><br>
		
	
	
	</form>
	</div>
</body>
</html>