<!-- FOOTER -->
			<footer id="footer">
				<div class="container">

					<div class="row">
						
						<div class="col-md-6">
							<!-- Footer Logo -->
							<img class="footer-logo" src="../../../assets/images/racni/footer/logo_footer_racni.png" alt="" />

							<!-- Small Description -->
							<p></p>

							<!-- Contact Address -->
							<address>
								<ul class="list-unstyled">
									<li class="footer-sprite address">
										<b>Sede:</b> Universidad Militar Nueva Granada Carrera 11 Bogotá, Colombia [<a href="http://www.umng.edu.co" target="_blank">www.umng.edu.co</a>]<br>
									</li>
									<li class="footer-sprite phone">
										<b>Tel:</b>  ext: 
									</li>
									<li class="footer-sprite email">
										<a href="#">racni2019@uniempresarial.edu.co</a>
									</li>
								</ul>
							</address>
							<!-- /Contact Address -->

						</div>

						<div class="col-md-6">


							<h4 class="letter-spacing-1">Sede</h4>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31813.081106992227!2d-74.06919479389539!3d4.658980033403383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9a977b6502c9%3A0x502b2e1ab14b506f!2sUniversidad+Militar+Nueva+Granada!5e0!3m2!1ses-419!2smx!4v1565649330637!5m2!1ses-419!2smx" width="450" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>

							<!-- Social Icons -->
							<div class="margin-top-20">
								<a href="https://www.facebook.com/Racni-AC-1695535400748027/" target="_blank" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">

									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>
					
							</div>
							<!-- /Social Icons -->

						</div>

					</div>

				</div>

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li><a href="../../../terminos-y-condiciones.html" target="_blank">Terminos &amp; Condiciones</a></li>
							<!-- <li>&bull;</li> punto intermedio -->
						</ul>
						&copy; Todos los Derechos Reservados, RACNI
					</div>
				</div>
			</footer>
			<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = '../../../assets/plugins/';</script>
		<script type="text/javascript" src="../../../assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="../../../assets/js/scripts.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/view/demo.revolution_slider.js"></script>
		
		<script>
			function openA(evt, AName) {
			    var i, tabcontent, tablinks;
			    tabcontent = document.getElementsByClassName("tabcontent");
			    for (i = 0; i < tabcontent.length; i++) {
			        tabcontent[i].style.display = "none";
			    }
			    tablinks = document.getElementsByClassName("tablinks");
			    for (i = 0; i < tablinks.length; i++) {
			        tablinks[i].className = tablinks[i].className.replace(" active", "");
			    }
			    document.getElementById(AName).style.display = "block";
			    evt.currentTarget.className += " active";
			}
		</script>

	</body>
</html>