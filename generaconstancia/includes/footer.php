<!-- FOOTER -->
			<footer id="footer">
				<div class="container">

					<div class="row">
						
						<div class="col-md-4">

							<!-- Small Description -->
							<p></p>

							<!-- Contact Address -->
							<address>
								<ul class="list-unstyled">
									<li>
										<b>Ubicación:</b> Departamento de Proyección Empresarial
									</li>
									<li>
										<b>Web:</b> <a href="http://www.incubadora.acatlan.unam.mx" target="_blank">www.incubadora.acatlan.unam.mx</a>
									</li>
									<li>
										<b>Tel:</b>  ext: 
									</li>
									<li>
										<b>Correo: </b><a href="proyeccion.empresarial.acatlan@gmail.com">proyeccion.empresarial.acatlan@gmail.com</a>
									</li>
								</ul>
							</address>
							<!-- /Contact Address -->

						</div>
						<div class="col-md-4">
							<!-- Social Icons -->
							<div class="margin-top-20">
								<a href="https://www.facebook.com/Incubadora-Acatlan/" target="_blank" class="social-icon social-icon-border social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="icon-facebook"></i>
								</a>
					
							</div>
							<!-- /Social Icons -->
						</div>

						<div class="col-md-4">


							<h4 class="letter-spacing-1">Ubicación</h4>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3761.4068811918664!2d-99.24920348567518!3d19.481122886856713!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2030f36b5cd89%3A0x1b282d5c027193c9!2sDepartamento%20de%20Proyeccion%20Empresarial!5e0!3m2!1ses-419!2smx!4v1575419443073!5m2!1ses-419!2smx" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

							

						</div>

					</div>

				</div>

				<div class="copyright">
					<div class="container">
						<ul class="pull-right nomargin list-inline mobile-block">
							<li><a href="../../../terminos-y-condiciones.html" target="_blank">Terminos &amp; Condiciones</a></li>
							<!-- <li>&bull;</li> punto intermedio -->
						</ul>
						&copy; Todos los Derechos Reservados, DPEICI
					</div>
				</div>
			</footer>
			<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = '../../../assets/plugins/';</script>
		<script type="text/javascript" src="../../../assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="../../../assets/js/scripts.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/view/demo.revolution_slider.js"></script>
		
		<script>
			function openA(evt, AName) {
			    var i, tabcontent, tablinks;
			    tabcontent = document.getElementsByClassName("tabcontent");
			    for (i = 0; i < tabcontent.length; i++) {
			        tabcontent[i].style.display = "none";
			    }
			    tablinks = document.getElementsByClassName("tablinks");
			    for (i = 0; i < tablinks.length; i++) {
			        tablinks[i].className = tablinks[i].className.replace(" active", "");
			    }
			    document.getElementById(AName).style.display = "block";
			    evt.currentTarget.className += " active";
			}
		</script>

	</body>
</html>