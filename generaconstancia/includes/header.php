<script type="text/javascript">
var texto_original = '';
function gestionarTexto(div)
{
//aquí valoramos si hay que expandir o contraer el texto, en función de lo que ponga en el <DIV>
if(div.innerHTML == 'Volver')
{contraer();div.innerHTML='Leer más';}
else
{expandir();div.innerHTML='Volver'}
}
function contraer()
{
//vamos a limitar el texto a 50 caracteres y guardamos el texto original
texto_original = document.getElementById('el_div').innerHTML;
document.getElementById('el_div').innerHTML = texto_original.substring(0,50) + '...';
}
function expandir()
{
document.getElementById('el_div').innerHTML=texto_ original;
}
</script>


<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html>   <!--<![endif]-->
	
	<head>
		<meta charset="utf-8" />
		<title>DPEICI</title>
		<meta name="keywords" content="RACNI, Red Academica de Comercio" />
		<meta name="description" content="" />
		<meta name="Author" content="SS" />

		<!-- FAVICON -->
		<link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon/favicon-32x32.png">

		<!-- mobile settings -->
		<!--<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		-->
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=no" />

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="../../../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="../../../assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="../../../assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="../../../assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="../../../assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
         <!--Google Font-->
		<link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">
	</head>

	<style>
		table {
    		border-collapse: collapse;
    		border-spacing: 0;
    		width: 100%;
    		border: 1px solid #ddd;
		}

		th, td {
    		text-align: left;
    		padding: 8px;
		}

		tr:nth-child(even){background-color: #f2f2f2}

		/* Style the tab */
		.tab {
		    overflow: hidden;
		    border: 1px solid #ccc;
		    background-color: #f1f1f1;
		}

		/* Style the buttons inside the tab */
		.tab button {
		    background-color: inherit;
		    float: left;
		    border: none;
		    outline: none;
		    cursor: pointer;
		    padding: 14px 16px;
		    transition: 0.3s;
		    font-size: 17px;
		}

		/* Change background color of buttons on hover */
		.tab button:hover {
		    background-color: #ddd;
		}

		/* Create an active/current tablink class */
		.tab button.active {
		    background-color: #ccc;
		}

		/* Style the tab content */
		.tabcontent {
		    display: none;
		    padding: 6px 12px;
		    border: 1px solid #ccc;
		    border-top: none;
		}
		.tabcontent1 {		    
		    padding: 6px 12px;
		    border: 1px solid #ccc;
		    border-top: none;
		}
		#textover
		{

			white-space: initial;
	 		overflow:hidden;
    		text-overflow:ellipsis;
    		width: 300px; 
    		height: 150px;
		}

		#textover:hover {
  			width: 300px;
			height: 100px;
  			white-space: initial;
  			overflow:visible;
  			cursor: pointer;
		}
		#DivRegistro {
			width: 400px;
			border-radius: 5px;
  			background-color: #f3f3f3;
  			padding: 20px;
		}
		#SelectDivRegistro{
			width: 100%;
  			padding: 10px 15px;
  			margin: 8px 0;
  			display: inline-block;
  			border: 1px solid #ccc;
  			border-radius: 4px;
  			box-sizing: border-box;
		}
		#InputDivRegistro{
			width: 100%;
  			padding: 10px 15px;
  			margin: 8px 0;
  			display: inline-block;
  			border: 1px solid #ccc;
  			border-radius: 4px;
  			box-sizing: border-box;
		}
	</style>






	<body class="smoothscroll enable-animation">


		<!-- wrapper -->
		<div id="wrapper">

			<div id="header" class="sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- Logo -->
						<a class="logo pull-left" href="../index.php">
							<img src="../assets/images/logos/logo.png" alt="" />
						</a>

						<div class="navbar-collapse pull-right nav-main-collapse collapse">
							<nav class="nav-main">

								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="active"><!-- INICIO -->
										<a class="active" href="../index.php">
											INICIO
										</a>
									</li>


								</ul><!-- FIN ul principal --> 

							</nav>
						</div>

					
				</header>
				<!-- /Top Nav -->

			</div>
	
