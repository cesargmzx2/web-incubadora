<?php
  require_once("conexionBD.php");

  $id = $_GET["id"];
  $idEvento = $_GET["evento"];
  $idUusuario_repetido;

  //obtenemos nombre del usuario a partir de su $id
  $sql = "SELECT nombre FROM Usuario WHERE id_usuario = '$id'";
  try {
        $result = $conn -> query($sql);
        $arreglo = $result-> fetch_array();
        //guardamos el atributo nombre de la tabla Usuario en la variable $nombreUsuario  
        $nombreUsuario = $arreglo["nombre"];

      } catch (Exception $e) { $respuesta1 = array("respuesta" => $e->getMessage()); }

  //bucamos el id_usuario en la tabla Asistencia que coincida con el $id ingresado a la aplicacion

  $sql = "SELECT id_usuario FROM Asistencia WHERE  id_usuario = '$id' AND id_evento = '$idEvento'";

  try {

    $result = $conn -> query($sql);
    $arreglo = $result->fetch_array();
    
    //Guardamos el id_usuario que esta repetido para poder comparar y realizar despues la comparacion
    $idUusuario_repetido = $arreglo["id_usuario"];

  } catch (Exception $e) { $respuesta = array( "respuesta" => $e->getMessage()); }

  //Aqui realizamos la condicional si el campo id no esta vacio y si el id no esta ya registrado en el evento
if (isset($id) && $id != $idUusuario_repetido)
{

  try {

        $stmt = $conn -> prepare("INSERT INTO Asistencia (id_evento, id_usuario) VALUES (?,?)");
        $stmt -> bind_param("ii", $idEvento, $id);
        $stmt -> execute();
        $respuesta = array(array('respuesta' => $nombreUsuario ));

      } catch (Exception $e) { $respuesta = array( "error" => $e->getMessage());} 

}else if(is_null($id)){ 
	$respuesta = array(array('respuesta' => "No estas en BD" ));
     }else{
	$respuesta = array(array('respuesta' => "Ya estas registrado" ));
}
echo is_null($respuesta1) ? 1 : 0;
//echo json_encode($respuesta);

  $result -> close();
  $conn -> close();
  $stmt -> close();

 ?>
